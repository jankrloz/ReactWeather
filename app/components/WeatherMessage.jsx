var React = require('react');

var WeatherMessage = ({temp, location}) => {
    return (
        <h1>{temp}°C en {location}</h1>
    )
};

module.exports = WeatherMessage;
